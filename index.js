const express = require('express');
const port = process.env.PORT;
// const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

// Permet de gérer les requêtes venant d'un domaine différent.
app.use(cors());

// Utilisé pour parser le contenu des requêtes POST
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());

// Importer les routes déclarées dans le routeur
app.use(require('./routes'));

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})