// Products Model

const {products} = require('../data.json');

// Retourne la liste de tous les produits
const getProducts = () => {
    return products;
}

// Retourne le produit correspondant au code produit
const getProductByCode = (code) => {
    return products.find(product => { // On va chercher et retourner uniquement le produit qui correspond au code demandé
        return product.product_code === code;
    });
}

module.exports = {
    getProducts,
    getProductByCode,
}
