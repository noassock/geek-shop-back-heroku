// Routes

const router = require('express').Router();

// Importer les middlewares afin de les assigner aux routes désirées
const middlewares = require('./middlewares.js')

// Importer les models qui renvoient les informations de la "BDD"
const productModel = require('./models/Products.js')
const vouchersModel = require('./models/Vouchers.js')

// Base route
/* Déclaration d'une route :
 - Verbe HTTP (GET, POST, PUT, DELETE)
 - Middleware
 - Callback
 */
router.get('/', middlewares.logRouteCall, (req, res, next) => {
    res.send('Accueil')
});

// Products routes
router.get('/products', middlewares.logRouteCall, (req, res, next) => {
    res.json(productModel.getProducts());
});

router.get('/products/:code', middlewares.logRouteCall, (req, res, next) => {
    if (req.params) { // On retrouve les paramètres de l'url dans req.params, ici "code"
        const product_code = req.params.code;
        res.json(productModel.getProductByCode(product_code));
    } else {
        res.send('Merci de fournir un code produit')
    }
});


// Voucher routes
router.get('/vouchers', (req, res, next) => {
    res.json(vouchersModel.getVouchers());
});

router.get('/vouchers/:name', (req, res, next) => {
    if (req.params) { // On retrouve les paramètres de l'url dans req.params, ici "name"
        const voucherName = req.params.name;
        res.json(vouchersModel.getVoucherValueByName(voucherName));
    } else {
        res.send('Merci de fournir un code produit')
    }
});

module.exports = router;
